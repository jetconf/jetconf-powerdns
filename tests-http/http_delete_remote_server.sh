#!/bin/bash

source ./http_conf

# This will delete remote-server from resolvers's configuration

DEL_SERVER=$1

if [ -z "$DEL_SERVER" ]
then
    echo "Missing argument: use name of remote-server as argument"
else
    echo "--- DELETE ${DEL_SERVER} from config"
    URL="${JC_URL}/restconf/data/cznic-dns-slave-server:dns-server/remote-server=${DEL_SERVER}"
    curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X DELETE "$URL"

    echo "--- POST conf-commit"
    URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
    curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
fi