#!/bin/bash

source ./http_conf

# This will delete zone from resolvers's configuration

DEL_ZONE=$1

if [ -z "$DEL_ZONE" ]
then
    echo "Missing argument: use domain of zone as argument"
else
    echo "--- DELETE ${DEL_ZONE} from zone list configuration"
    URL="${JC_URL}/restconf/data/cznic-dns-slave-server:dns-server/zones/zone=${DEL_ZONE}"
    curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X DELETE "$URL"

    echo "--- POST conf-commit"
    URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
    curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"
fi


