#!/bin/bash

source ./http_conf

# rpc operation restart server

POST_DATA="@payload/reload-server-input.json"
URL="${JC_URL}/restconf/operations/"

echo "--- GET Operation 'restart-server'"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X GET "$URL"
