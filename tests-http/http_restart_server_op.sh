#!/bin/bash

source ./http_conf

# rpc operation restart server

POST_DATA='{"cznic-dns-slave-server:input": {}}'
URL="${JC_URL}/restconf/operations/cznic-dns-slave-server:restart-server"

echo "--- POST Operation 'restart-server'"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"
