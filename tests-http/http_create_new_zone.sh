#!/bin/bash

source ./http_conf

# Updating zones list,

POST_DATA="@payload/zone-input.json"

echo "--- POST new zone to zone list configuration"
URL="${JC_URL}/restconf/data/cznic-dns-slave-server:dns-server/zones"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST -d "$POST_DATA" "$URL"

echo "--- POST conf-commit"
URL="${JC_URL}/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E $CLIENT_CERT -X POST "$URL"



