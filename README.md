# PowerDNS Jetconf backend

JetConf backend for the PowerDNS authoritative server 

## Development
[Information](https://gitlab.labs.nic.cz/labs/jetconf/wikis/development) for developers

## Prerequisites

[Python](https://www.python.org/) 3.6 or newer is required and [PowerDNS](https://www.powerdns.com/) should be installed


* [jetconf](https://gitlab.labs.nic.cz/labs/jetconf)


## Installation

```bash
$ python setup.py install
```

### Allow running powerdns using systemd as non-root user
Create a new group, for example
`pdnscontrol`
```bash
$ sudo groupadd pdnscontrol
```
Add `jetconf` user to this group.
```bash
$ usermod -a -G pdnscontrol jetconf
```
Create the new sudoers configuration file in `sudoers.d` directory
```bash
$ sudo nano /etc/sudoers.d/pdnscontrol
```
Add these rules to this file 
```bash
Cmnd_Alias PDNS_CMDS = /bin/systemctl start pdns, /bin/systemctl stop pdns, /bin/systemctl reload pdns, /bin/systemctl restart pdns
%pdnscontrol ALL=(ALL) NOPASSWD: PDNS_CMDS
```
Now every user, which is member of `pdnscontrol` group cal execute `systemctl` commands for `pdns` with `sudo` and sudo password is not needed.

## Running with JetConf

Example YAML configuration file, certificates and json-data are located in `conf` folder.
More on [JetConf wiki](https://gitlab.labs.nic.cz/labs/jetconf/wikis/home).
```bash
$ jetconf -c config-example.yaml
```