import json

from colorlog import error, info
from jetconf.helpers import LogHelpers

from .pdns_lib import ApiClient, ZonesApi, ServersApi, ZonemetadataApi
from .pdns_lib.rest import ApiException
from .pdns_lib import models

PDNS = None  # type: PdnsClient

debug_soh = LogHelpers.create_module_dbg_logger(__name__)


class PdnsClient(ApiClient):

    def __init__(self, configuration, header_name=None, header_value=None, cookie=None):
        super().__init__(configuration, header_name, header_value, cookie)

        self._zones_api = ZonesApi(ApiClient(configuration))
        self._zonemetadata_api = ZonemetadataApi(ApiClient(configuration))
        self._servers_api = ServersApi(ApiClient(configuration))

    def delete_remote_server(self, remote_server, server_id):

        try:
            pdns_zones = self._zones_api.list_zones(server_id)
            debug_soh(self.__class__.__name__ + "List all remote-servers, response: {}".format(pdns_zones))
        except ApiException as e:
            error("List all remote-servers, PowerDNS API error: {}".format(e))
            return

        ip_address = remote_server['remote']['ip-address']
        if 'port' in remote_server['remote']:
            if ':' in ip_address:
                ip_address += "[" + ip_address + "]:" + str(remote_server['remote']['port'])
            else:
                ip_address += ":" + str(remote_server['remote']['port'])

        for zone in pdns_zones:
            new_masters = zone.masters
            for master in new_masters:
                if master == ip_address:
                    new_masters.remove(ip_address)

            if not zone.masters:
                new_masters = [""]

            try:
                api_response = self._zones_api.put_zone(
                    server_id=server_id,
                    zone_id=zone.id,
                    zone_struct={"masters": new_masters}
                )

                debug_soh(self.__class__.__name__ + "delete remote-server, response: {}".format(api_response))
                return api_response

            except ApiException as e:
                error("PowerDNS API error: {}".format(e))

    def delete_zone(self, zone_id, server_id):
        try:
            api_response = self._zones_api.delete_zone(server_id, zone_id)
            debug_soh(self.__class__.__name__ + "delete zone '{}', response: {}".format(zone_id, api_response))
            return api_response

        except ApiException as e:
            error("Delete zone '{}', PowerDNS API error: {}".format(zone_id, e))

    def delete_all_zones(self, serve_id):

        pdns_zones = self._zones_api.list_zones(serve_id)

        for zone in pdns_zones:

            zone_id = zone.id
            self.delete_zone(zone_id, serve_id)

    def create_zone(self, zone_dict, remote_servers, server_id):

        zone_data = models.Zone(
            name=zone_dict['domain'],
            type="Zone",
            kind=str(zone_dict['role']).capitalize()
        )

        zone_metadata = []
        zone_masters = zone_dict['master']
        zone_recipients = zone_dict['notify']['recipient']

        if zone_masters:
            masters_ip = []

            for master in zone_masters:
                for server in remote_servers:
                    if master == server['name']:
                        ip_address = server['remote']['ip-address']
                        if 'port' in server['remote']:
                            if ':' in ip_address:
                                ip_address = "[" + ip_address + "]:" + str(server['remote']['port'])
                            else:
                                ip_address = ip_address + ":" + str(server['remote']['port'])
                        masters_ip.append(ip_address)

            zone_data.masters = masters_ip

        if zone_recipients:
            recipients_ip = []

            for recipient in zone_recipients:
                for server in remote_servers:
                    if recipient == server['name']:
                        ip_address = server['remote']['ip-address']
                        if 'port' in server['remote']:
                            if ':' in ip_address:
                                ip_address = "[" + ip_address + "]:" + str(server['remote']['port'])
                            else:
                                ip_address += ":" + str(server['remote']['port'])
                        recipients_ip.append(ip_address)

            zone_metadata.append(models.Metadata(
                kind="ALSO-NOTIFY",
                metadata=recipients_ip
            ))

        try:
            api_response = self._zones_api.create_zone(
                server_id=server_id,
                zone_struct=zone_data
            )

            debug_soh(self.__class__.__name__ + "create zone, response: {}".format(api_response))

            if zone_metadata:

                for data in zone_metadata:
                    api_metadata_response = self._zonemetadata_api.create_metadata(
                        server_id=server_id,
                        zone_id=zone_dict['domain'],
                        metadata=data
                    )

                    debug_soh(self.__class__.__name__ + "create zone metadata, response: {}"
                              .format(api_metadata_response))

        except ApiException as e:
            error("PowerDNS API error: {}".format(e))

    def set_config(self, server_id, config):
        if 'cznic-dns-slave-server:dns-server' in config:
            config = config['cznic-dns-slave-server:dns-server']

        self.delete_all_zones(server_id)

        remote_servers = config['remote-server']
        for zone in config['zones']['zone']:
            self.create_zone(
                server_id=server_id,
                zone_dict=zone,
                remote_servers=remote_servers
            )

    def load_config(self, server_id):
        try:
            pdns_zones = self._zones_api.list_zones(server_id)
        except ApiException as e:
            error("Load PowerDNS config failed, PowerDNS API error: {}".format(e))
            return

        loaded_config = {
            "remote-server": [],
            "zones": {"zone": []}
        }

        loaded_zones = []
        loaded_servers = []

        i = 0
        for pdns_zone in pdns_zones:
            new_masters = []
            new_recipients = []
            zone_metadata = []

            try:
                zone_metadata = self._zonemetadata_api.list_metadata(server_id, pdns_zone.id)
            except ApiException as e:
                error("Load PowerDNS '{}' zone metadata failed, PowerDNS API error: {}"
                      .format(pdns_zone.id, e))

            if zone_metadata:
                for meta in zone_metadata:
                    if meta.kind == 'ALSO-NOTIFY':
                        for address in meta.metadata:
                            i += 1
                            server = {
                                "name": ("remote-" + str(i)),
                                "description": "remote-server-{} loaded from PowerDNS".format(i),
                                "remote": {
                                    "ip-address": str,
                                }
                            }

                            if address.count(":") is 1:
                                addr, port = address.split(':')
                                server['remote']['port'] = int(port)
                            elif ']:' in address:
                                addr, port = address.split(']:')
                                addr = addr[1:]
                                server['remote']['port'] = int(port)
                            else:
                                addr = address
                            server['remote']['ip-address'] = addr

                            zrecipient = server['name']
                            for lserver in loaded_servers:
                                if addr == lserver['remote']['ip-address']:
                                    zrecipient = lserver['name']

                            if not any(lserver['name'] == zrecipient for lserver in loaded_servers):
                                loaded_servers.append(server.copy())

                            new_recipients.append(zrecipient)

            for master in pdns_zone.masters:
                i += 1

                server = {
                    "name": ("remote-" + str(i)),
                    "description": "remote-server-{} loaded from PowerDNS".format(i),
                    "remote": {
                        "ip-address": str,
                    }
                }

                if master.count(":") is 1:
                    addr, port = master.split(':')
                    server['remote']['port'] = int(port)
                elif ']:' in master:
                    addr, port = master.split(']:')
                    addr = addr[1:]
                    server['remote']['port'] = int(port)
                else:
                    addr = master
                server['remote']['ip-address'] = addr

                zmaster = server['name']
                for lserver in loaded_servers:
                    if addr == lserver['remote']['ip-address']:
                        zmaster = lserver['name']

                if not any(lserver['name'] == zmaster for lserver in loaded_servers):
                    loaded_servers.append(server.copy())

                new_masters.append(zmaster)

            zone = {
                "domain": pdns_zone.name,
                "description": (pdns_zone.name + " zone"),
                "master": new_masters,
                "notify": {
                    "recipient": new_recipients
                }
            }

            if pdns_zone.kind is 'Master':
                zone['role'] = "master"

            loaded_zones.append(zone.copy())

        loaded_config['remote-server'] = loaded_servers.copy()
        loaded_config['zones']['zone'] = loaded_zones.copy()

        return {"cznic-dns-slave-server:dns-server": loaded_config}










