from enum import Enum
import subprocess
from typing import Dict, List
from colorlog import error, warning as warn, info

from . import pdns_client as so
from jetconf import config
from jetconf.helpers import JsonNodeT, LogHelpers
from jetconf.data import BaseDatastore

debug_oph = LogHelpers.create_module_dbg_logger(__name__)

# ---------- User-defined handlers follow ----------


class OpHandlersContainer:
    def __init__(self, ds: BaseDatastore):
        self.ds = ds

    def reload_server_op(self, input_args: JsonNodeT, username: str) -> JsonNodeT:
        debug_oph("reload-server called")

        info("Job type reload is not applicable for unit pdns.service.")
        # try:
        #     res = subprocess.check_output(["sudo", "systemctl", "reload", "pdns"], timeout=10).decode('utf-8')
        # except subprocess.TimeoutExpired as te:
        #     res = str(te)
        #
        # if res is "":
        #     info("PowerDNS has been reloaded, user: {}".format(username))
        #     info("Setting datastore configuration to PowerDNS")
        #     so.PDNS.set_config(
        #         server_id=config.CFG.root['PDNS']['SERVER_ID'],
        #         config=self.ds.get_data_root().add_defaults().value
        #     )
        # else:
        #         error("PowerDNS reload failed, reason: {}".format(res))

    def restart_server_op(self, input_args: JsonNodeT, username: str) -> JsonNodeT:
        debug_oph("restart-server called")

        try:
            res = subprocess.check_output(["sudo", "systemctl", "restart", "pdns"], timeout=10).decode('utf-8')
        except subprocess.TimeoutExpired as te:
            res = str(te)

        if res is "":
            info("PowerDNS has been restarted, user: {}".format(username))

            info("Setting datastore configuration to PowerDNS")
            so.PDNS.set_config(
                server_id=config.CFG.root['PDNS']['SERVER_ID'],
                config=self.ds.get_data_root().add_defaults().value
            )
        else:
                error("PowerDNS restart failed, reason: {}".format(res))

    def start_server_op(self, input_args: JsonNodeT, username: str) -> JsonNodeT:
        debug_oph("start-server called")

        try:
            res = subprocess.check_output(["sudo", "systemctl", "start", "pdns"], timeout=10).decode('utf-8')
        except subprocess.TimeoutExpired as te:
            res = str(te)

        if res is "":
            info("PowerDNS has been started, user: {}".format(username))

            info("Setting datastore configuration to PowerDNS")
            so.PDNS.set_config(
                server_id=config.CFG.root['PDNS']['SERVER_ID'],
                config=self.ds.get_data_root().add_defaults().value
            )
        else:
            error("PowerDNS start failed, reason: {}".format(res))

    def stop_server_op(self, input_args: JsonNodeT, username: str) -> JsonNodeT:
        debug_oph("stop-server called")

        try:
            res = subprocess.check_output(["sudo", "systemctl", "stop", "pdns"], timeout=10).decode('utf-8')
        except subprocess.TimeoutExpired as te:
            res = str(te)

        if res is "":
            info("PowerDNS has been stopped, user: {}".format(username))
        else:
            error("PowerDNS stop failed, reason: {}".format(res))


def register_op_handlers(ds: BaseDatastore):
    op_handlers_obj = OpHandlersContainer(ds)
    ds.handlers.op.register(op_handlers_obj.reload_server_op, "cznic-dns-slave-server:reload-server")
    ds.handlers.op.register(op_handlers_obj.restart_server_op, "cznic-dns-slave-server:restart-server")
    ds.handlers.op.register(op_handlers_obj.start_server_op, "cznic-dns-slave-server:start-server")
    ds.handlers.op.register(op_handlers_obj.stop_server_op, "cznic-dns-slave-server:stop-server")
