from typing import List, Dict, Union, Any

from yangson.instance import InstanceRoute, ObjectValue, EntryKeys

from jetconf import config
from jetconf.data import BaseDatastore, ChangeType, DataChange
from jetconf.helpers import ErrorHelpers, LogHelpers
from jetconf.handler_base import ConfDataObjectHandler, ConfDataListHandler

from . import pdns_client as so

JsonNodeT = Union[Dict[str, Any], List]
epretty = ErrorHelpers.epretty
debug_confh = LogHelpers.create_module_dbg_logger(__name__)


# ---------- User-defined handlers follow ----------

class RemoteHandler(ConfDataListHandler):

    def create_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create item triggered")

    def create_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create list triggered")

    def replace_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace item triggered")

        root_data = self.ds.get_data_root().add_defaults().value
        so.PDNS.set_config(
            server_id=config.CFG.root['PDNS']['SERVER_ID'],
            config=root_data
        )

    def replace_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace list triggered")

    def delete_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete item triggered")

        # Delete remote-server
        if (len(ii) == 4) and isinstance(ii[3], EntryKeys) and (ch.change_type == ChangeType.DELETE):
            name = ii[2].keys[("name", None)]
            if name is not None:
                debug_confh("Deleting zone \"{}\"".format(name))
                so.PDNS.delete_remote_server(
                    server_id=config.CFG.root['PDNS']['SERVER_ID'],
                    remote_server=name
                )

    def delete_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete list triggered")


class ZoneHandler(ConfDataListHandler):

    def create_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create item triggered")

        root_data = self.ds.get_data_root().add_defaults().value
        so.PDNS.set_config(
            server_id=config.CFG.root['PDNS']['SERVER_ID'],
            config=root_data
        )

    def create_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create list triggered")

    def replace_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace item triggered")

        root_data = self.ds.get_data_root().add_defaults().value
        so.PDNS.set_config(
            server_id=config.CFG.root['PDNS']['SERVER_ID'],
            config=root_data
        )

    def replace_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace list triggered")

    def delete_item(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete item triggered")

        # Delete zone
        if (len(ii) == 4) and isinstance(ii[3], EntryKeys) and (ch.change_type == ChangeType.DELETE):
            domain = ii[3].keys[("domain", None)]
            if domain is not None:
                debug_confh("Deleting zone \"{}\"".format(domain))
                so.PDNS.delete_zone(
                    server_id=config.CFG.root['PDNS']['SERVER_ID'],
                    zone_id=domain
                )

    def delete_list(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete list triggered")


def commit_begin():
    debug_confh("Connecing to PowerDNS http API")


def commit_end(failed: bool=False):

    if failed:
        debug_confh("Aborting PowerDNS transaction")
    else:
        debug_confh("Commiting PowerDNS transaction")


def register_conf_handlers(ds: BaseDatastore):
    ds.handlers.conf.register(RemoteHandler(ds, "/cznic-dns-slave-server:dns-server/remote-server"))
    ds.handlers.conf.register(ZoneHandler(ds, "/cznic-dns-slave-server:dns-server/zones/zone"))

    # Set datastore commit callbacks
    ds.handlers.commit_begin = commit_begin
    ds.handlers.commit_end = commit_end
