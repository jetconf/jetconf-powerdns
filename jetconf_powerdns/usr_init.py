from colorlog import error, info

from jetconf import config
from . import pdns_client as so


def jc_startup():
    info("Backend: init")


def jc_end():
    info("Backend: cleaning up")
    so.PDNS = None
