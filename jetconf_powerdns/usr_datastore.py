import json

from colorlog import error, info
from yangson.instance import NonexistentInstance
from jetconf import config
from jetconf.data import JsonDatastore
from jetconf.helpers import ErrorHelpers

from . import pdns_client as so
from .pdns_client import PdnsClient
from . import pdns_lib


class UserDatastore(JsonDatastore):
    def load(self):
        super().load()

        # when using old version of JetConf which is not loading PDNS config from YAML
        # config.CFG.root['PDNS'] = {"LOAD_CONF": False, "API_KEY": "powerdns"}

        # Configure API key authorization: APIKeyHeader
        configuration = pdns_lib.Configuration()
        configuration.api_key['X-API-Key'] = config.CFG.root['PDNS']['API_KEY']

        # create an instance of the API class
        so.PDNS = PdnsClient(configuration)

        if config.CFG.root['PDNS']['LOAD_CONF']:
            # Read PowerDNS configuration and save it to the datastore
            info("Loading configuration from PowerDNS")
            pdns_conf_json = so.PDNS.load_config(config.CFG.root['PDNS']['SERVER_ID'])
            new_root = self._data.put_member("cznic-dns-slave-server:dns-server",
                                             pdns_conf_json["cznic-dns-slave-server:dns-server"],
                                             raw=True).top()
            self.set_data_root(new_root)

        else:
            root_data = self.get_data_root()
            if "cznic-dns-slave-server:dns-server" in root_data:

                # Set datastore configuration to PowerDNS
                info("Setting datastore configuration to PowerDNS")
                so.PDNS.set_config(
                    server_id=config.CFG.root['PDNS']['SERVER_ID'],
                    config=root_data.add_defaults().value
                )
            else:
                info("No configuration for PowerDNS was found in datastore")

    def save(self):
        if config.CFG.root['PDNS']['LOAD_CONF']:
            # Just need to save NACM data,
            # everything else is loaded dynamically on startup or when requested
            try:
                nacm_raw = self._data["ietf-netconf-acm:nacm"].raw_value()
                data_raw = {"ietf-netconf-acm:nacm": nacm_raw}
            except NonexistentInstance:
                # NACM data not present
                data_raw = {}

            with open(self.json_file, "w") as jfd:
                json.dump(data_raw, jfd, indent=4)

        else:
            super().save()
